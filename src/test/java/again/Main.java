package again;

public class Main {

    public static void main(String[] args) {
        Thread t1 = new Thread(new StartThread());
        Thread t2 = new Thread(new StopThread());
        t1.start();
        t2.start();
    }
}
