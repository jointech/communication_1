import module.modbus.tcp.TcpStartThread;

public class StopThread {

    public static void main(String[] args) {

       new Thread(new Runnable() {
           @Override
           public void run() {
               TcpThread.isRunning = false;
               TcpThread.count = 500;
               System.out.println("当前状态:"+TcpThread.isRunning);
           }
       }).start();


    }
}
