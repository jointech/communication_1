package charset;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class CharsetTest {

    @Test
    public void test1(){

        String  en = "abcdefghijklmnopqrstuvwxyz";
        byte[] bytes = en.getBytes();
        System.out.println(Arrays.toString(bytes));
        try {
            bytes = en.getBytes("utf-8");
            System.out.println(Arrays.toString(bytes));
            bytes = en.getBytes("utf-16");
            System.out.println(Arrays.toString(bytes));
            bytes = en.getBytes("GBK");
            System.out.println(Arrays.toString(bytes));
            bytes = en.getBytes("GB2312");
            System.out.println(Arrays.toString(bytes));
            bytes = en.getBytes("ISO-8859-1");
            System.out.println(Arrays.toString(bytes));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
