package common;

public class MonitorHelper {

    public  static  void  pause(int time){

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
