package common;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/17
 */
public class Const {

    /**读功能码*/
    public static  final int  READ = 3;

    /**单个写功能码*/
    public static final  int  SINGLE_WRITE = 6;

    /**批量写功能码*/
    public static  final  int MULTI_WRITE = 16;

    public static String TCP_SEND_PREFIX="0000000000";

    public static  String HOSTlINK_PREFIX = "@";

    public static  String HOSTlINK_SUFFIX = "*\r";






}
