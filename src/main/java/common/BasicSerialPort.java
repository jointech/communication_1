package common;

import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;


/**
 * @author bingo
 * @Description
 * @Date 2018/8/18
 */
public abstract  class BasicSerialPort {


    protected SerialPort serialPort;

    protected OutputStream outputStream;

    protected InputStream inputStream;

    protected  static  int  baudRate = 9600;

    private Logger logger = LoggerFactory.getLogger(BasicSerialPort.class);

    public abstract  String receive();

    public class SerialPortListener implements SerialPortEventListener {

        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            switch (serialPortEvent.getEventType()) {

                case SerialPortEvent.BI:
                  logger.info("通讯中断");
                    break;
                case SerialPortEvent.CD:
                  logger.info("载波检测");
                    break;
                case SerialPortEvent.CTS:
                  logger.info("清除发送");
                    break;
                case SerialPortEvent.DATA_AVAILABLE:
                    logger.info("有数据到达哦~");
                    receive();
                    break;
                case SerialPortEvent.DSR:
                  logger.info("数据设备准备好");
                    break;
                case SerialPortEvent.FE:
                  logger.info("帧错误");
                    break;
                case SerialPortEvent.OE:
                  logger.info("溢位错误");
                    break;
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                  logger.info("输出缓冲区已清空");
                    break;
                case SerialPortEvent.PE:
                  logger.info("奇偶校验错误");
                    break;
                case SerialPortEvent.RI:
                  logger.info("响铃侦测");
                default:
                    break;
            }

        }

    }


}
