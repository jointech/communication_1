package module.mmall.dao;

import java.util.List;
import module.mmall.pojo.PayInfo;

public interface PayInfoMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(PayInfo record);

    PayInfo selectByPrimaryKey(Integer id);

    List<PayInfo> selectAll();

    int updateByPrimaryKey(PayInfo record);
}