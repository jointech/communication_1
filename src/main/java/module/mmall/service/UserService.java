package module.mmall.service;

import module.mmall.dao.UserMapper;
import module.mmall.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author bingo
 * @Description* @Date 2018/9/5
 */
public class UserService {

   private  SqlSessionFactory sqlSessionFactory;

   private SqlSession session ;

   public void  inint(){
        try {
            InputStream inputStream  = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void select(){
        session = sqlSessionFactory.openSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        List<User> userList = userMapper.selectAll();
        for(User user:userList){
            System.out.println(user);
        }
    }

    public void  close(){
       session.close();
        System.out.println("已经关闭");
    }

    @Test
    public void testSelect(){
        try {

            InputStream inputStream  = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);
            long startTime = System.currentTimeMillis();
            for(int i=0;i<10;i++){
                System.out.println("打开会话");
                session = sqlSessionFactory.openSession();
                UserMapper userMapper = session.getMapper(UserMapper.class);
                List<User> userList = userMapper.selectAll();
                System.out.println("第"+(i+1)+"次查询");
                session.close();
                System.out.println("关闭会话");
            }
            long endTime = System.currentTimeMillis();
            System.out.println((endTime-startTime)+"ms");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSelect2(){
       int i=0;
        try {

            InputStream inputStream  = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long startTime = System.currentTimeMillis();
        while (true){
            i++;
            long currentTime = System.currentTimeMillis();
            if(currentTime-startTime>=1000){
                break;
            }
            session = sqlSessionFactory.openSession();
            UserMapper userMapper = session.getMapper(UserMapper.class);
            List<User> userList = userMapper.selectAll();
            System.out.print("第"+i+"次查询:"+session);
            System.out.println((currentTime-startTime));
            session.close();
            System.out.println("关闭会话");
        }

    }
}
