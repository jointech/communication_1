package module.modbus.rtu;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class RtuStopThread implements Runnable {

    private ModbusRtu serialPortRtu;

    public RtuStopThread(ModbusRtu serialPortRtu) {
        this.serialPortRtu = serialPortRtu;
    }

    @Override
    public void run() {
        while(RtuStartThread.isLocked){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        serialPortRtu.closePort();
    }

}
