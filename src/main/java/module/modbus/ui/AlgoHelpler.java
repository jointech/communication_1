package module.modbus.ui;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * @author bingo
 * @Description
 * @Date 2018/4/24
 */
public class AlgoHelpler {

    public AlgoHelpler() {
    }

    public static  void setStrokeWeight(Graphics2D g2d,int weight){

        g2d.setStroke(new BasicStroke(weight,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
    }

    public static  void setColor(Graphics2D  g2d,Color color){
        g2d.setColor(color);
    }

    public static  void drawCircle(Graphics2D  g2d,int x,int y,int r){
        Ellipse2D circle = new Ellipse2D.Double(x,y,2*r,2*r);
        g2d.draw(circle);

    }

    public static  void fillCircle(Graphics2D  g2d,int x,int y,int r){
        Ellipse2D circle = new Ellipse2D.Double(x,y,2*r,2*r);
        g2d.fill(circle);
    }
    public static void fillRectangle(Graphics2D g, int x, int y, int w, int h){

        Rectangle2D rectangle = new Rectangle2D.Double(x, y, w, h);
        g.fill(rectangle);
    }
    public static  void pause(int t){
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
