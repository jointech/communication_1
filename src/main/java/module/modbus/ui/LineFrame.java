package  module.modbus.ui;
import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/7
 */
public class LineFrame extends JFrame {

    private int canvasWeight;

    private int canvasHeight;

    public  LineFrame(String title,int canvasWeight,int canvasHeight){
        super(title);
        this.canvasHeight = canvasHeight;
        this.canvasWeight = canvasWeight;

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
        LineCanvas lineCanvas = new LineCanvas();
        setContentPane(lineCanvas);
        this.pack();
        setResizable(false);
        data = new HashMap<>();
        data.put("temparature","xxxx");
        data.put("sendCount","xxxx");
    }
    private Map<String, String>   data ;
    private String showData;
    public void render(Map<String, String> data){
        this.data = data;
        repaint();
    }
    public void render( String showData ){
        this.showData = showData;
        repaint();
    }
    public LineFrame(String title)  {
        this(title,1208,768);
    }
    private class LineCanvas extends  JPanel{

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2D = (Graphics2D) g;
            AlgoHelpler.setStrokeWeight(g2D, 3);
            Font font = new Font("宋体",Font.BOLD,36);
            g2D.setFont(font);

            g2D.drawString("xxxxxxxx",100,100);
            g2D.drawString("xxxxxxxxx",100,150);
            }
        @Override
        public Dimension getPreferredSize() {
            return  new Dimension(canvasWeight,canvasHeight);
        }

    }

}
