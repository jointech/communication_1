package module.modbus.ui;


import module.modbus.rtu.ModbusRtu;
import util.SerialTool;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/7
 */
public class BrokenVisualizer {

    public static int sendCount = 0;

    private LineFrame  lineFrame ;

    private Map<String,String> dataMap = new HashMap<>();

    public BrokenVisualizer() {
     lineFrame = new LineFrame("温度实时监控",Const.horizontalLength+100,Const.verticalLength+100);
     new Thread(new Runnable() {

         @Override
         public void run() {
             dataMap.put("sendCount","xxxxx");
             dataMap.put("temparature","xxxxx");
             lineFrame.render(dataMap);
                 ModbusRtu modbusRtu  = new ModbusRtu();
                 modbusRtu.open(SerialTool.getCommPortIdentifier("COM5"));
                 while (true){
                     try {
                         Thread.sleep(500);
                     } catch (InterruptedException e) {
                         e.printStackTrace();
                     }
                     modbusRtu.send(1,3,0,new short[]{2});
                     sendCount++;
                     dataMap.put("sendCount",String.valueOf(sendCount));
                     dataMap.put("temparature",modbusRtu.getTemparature());
                     lineFrame.render(dataMap);
                 }

         }
     }).start();

    }
    public static void main(String[] args) {
        BrokenVisualizer brokenVisualizer = new BrokenVisualizer();
    }

}
