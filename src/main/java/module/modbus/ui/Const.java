package module.modbus.ui;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/7
 */
public class Const {

    /**水平坐标轴*/
    public static  int  horizontalLength=1511;
    /**数值坐标轴长度*/
    public static  int verticalLength = 809;
    /**x方向偏移*/
    public static  int xOffset = 50;
   /**y方向偏移*/
    public static  int yOffset = 50;


}
