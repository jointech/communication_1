package module.modbus.ui;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/7
 */
public class Point {

    private int x;

    private int y;

    private  int data;

    public Point(int x, int y, int data) {
        this.x = x;
        this.y = y;
        this.data = data;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
