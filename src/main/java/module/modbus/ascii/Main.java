package module.modbus.ascii;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class Main {

    public static void main(String[] args) {
        AsciiStartThread asciiStartThread = new AsciiStartThread();
        AsciiStopThread asciiStopThread = new AsciiStopThread(asciiStartThread.getModbusAscii());

        Thread t1 = new Thread(asciiStartThread);
        Thread t2 = new Thread(asciiStopThread);

        t1.start();
        t2.start();
    }
}
