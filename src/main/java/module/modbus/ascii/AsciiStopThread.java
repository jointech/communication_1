package module.modbus.ascii;


/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class AsciiStopThread implements  Runnable {

    private ModbusAscii modbusAscii;

    public AsciiStopThread(ModbusAscii modbusAscii) {
        this.modbusAscii = modbusAscii;
    }


    @Override
    public void run() {
        while (AsciiStartThread.isLock){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        modbusAscii.close();
    }
}
