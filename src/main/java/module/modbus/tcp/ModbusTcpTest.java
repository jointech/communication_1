package module.modbus.tcp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/17
 */
public class ModbusTcpTest {

    private static Logger logger = LoggerFactory.getLogger(ModbusTcpTest.class);

    public static void main(String[] args) {

        ModbusTcp modbusTcp = null;
        try {
            modbusTcp = new ModbusTcp("122.152.214.142",502);
        } catch (IOException e) {
            logger.info("建立连接失败！请确认连接信息");
            return;
        }
        for(int i=1;i<=3;i++){
        modbusTcp.send(1,16,0,new short[]{2,3,1});
      try {
          Thread.sleep(250);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
     // modbusTcp.receive();
  }
    }
}
