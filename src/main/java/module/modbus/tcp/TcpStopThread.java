package module.modbus.tcp;


import java.io.IOException;

public class TcpStopThread implements Runnable {

    private ModbusTcp modbusTcp ;

    public TcpStopThread(ModbusTcp modbusTcp) {
        this.modbusTcp = modbusTcp;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(36000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TcpStartThread.isRunning = false;
    }
}
