package module.modbus.tcp;


import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        ModbusTcp modbusTcp = null;
        try {
            modbusTcp = new ModbusTcp("122.152.214.142",502);
        } catch (IOException e) {
            e.printStackTrace();
        }
        TcpStartThread tcpStartThread = new TcpStartThread(modbusTcp);
         Thread thread = new Thread(tcpStartThread);
         thread.start();

         TcpStopThread tcpStopThread = new TcpStopThread(modbusTcp);
         Thread thread1 = new Thread(tcpStopThread);
         thread1.start();


    }
}
