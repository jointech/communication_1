package module.hostlink;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class Main {
    public static void main(String[] args) {
        HostLinkStartThread hostLinkStartThread = new HostLinkStartThread();
        HostLinkStopThead stopThead = new HostLinkStopThead(hostLinkStartThread.getHostLink());

        Thread t1 = new Thread(hostLinkStartThread);
        Thread t2 = new Thread(stopThead);
        t1.start();
        t2.start();
    }

}
