package module.hostlink;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class HostLinkStopThead implements Runnable {

    private HostLink hostLink;

    public HostLinkStopThead(HostLink hostLink) {
        this.hostLink = hostLink;
    }

    @Override
    public void run() {
        while(HostLinkStartThread.isLock){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        hostLink.closePort();
    }
}
