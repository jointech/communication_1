package module.hostlink;

import util.SerialTool;


/**
 * @author bingo
 * @Description
 * @Date 2018/8/22
 */
public class HostLinkStartThread implements  Runnable {

    public static  boolean isLock = true;

    private HostLink hostLink;

    public HostLinkStartThread() {
        hostLink = new HostLink();
    }

    public HostLink getHostLink() {
        return hostLink;
    }

    @Override
    public void run() {
        hostLink.open(SerialTool.getCommPortIdentifier("COM5"));
        hostLink.send("00","RD","0011",new short[]{2});

    }

}
