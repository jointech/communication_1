package module.usb;

import javax.usb.*;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/10
 */
public class Main {

    public static void main(String[] args) {
        try {
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootUsbHub = services.getRootUsbHub();
            try {
                System.out.println(rootUsbHub.getProductString());
               /* System.out.println(rootUsbHub.getUsbDeviceDescriptor());
                System.out.println(rootUsbHub.getNumberOfPorts());
                System.out.println(rootUsbHub.getSerialNumberString());*/
                List<UsbDevice> list = rootUsbHub.getAttachedUsbDevices();
                System.out.println(rootUsbHub);
                System.out.println("size:"+list.size());
                for(int i=0;i<list.size();i++){
                    System.out.println(list.get(i));
                }
        /*        System.out.println(list.get(0));
                System.out.println(list.get(1));
                System.out.println(list.get(2));
                System.out.println(list.get(3));
                System.out.println(list.get(4));*/
                // System.out.println(list.get(2));
               //System.out.println(list.get(4).getProductString());

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } catch (UsbException e) {
            e.printStackTrace();
        }

    }
}
