package module.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/10
 */
public class SerialPortService {

    private  List<String> coms = new ArrayList<>();
    private SerialPort serialPort;
    private List<CommPortIdentifier> comPorts = new ArrayList<>();
    private  static  int  baudRate = 9600;


    public SerialPortService() {
        CommPortIdentifier commPortIdentifier;
        Enumeration<CommPortIdentifier> comPortList = CommPortIdentifier.getPortIdentifiers();
        int i = 0;
        while (comPortList.hasMoreElements()) {
            commPortIdentifier = comPortList.nextElement();
            comPorts.add(commPortIdentifier);
            coms.add(commPortIdentifier.getName());
            i++;
        }
    }

    /**
     * 打开串口
     */
    public SerialPort openPort(CommPortIdentifier commPortIdentifier)
            throws PortInUseException, UnsupportedCommOperationException {

        serialPort = (SerialPort) commPortIdentifier.open(Object.class.getSimpleName(), 2000);
        serialPort.notifyOnDataAvailable(true);
        serialPort.setSerialPortParams(baudRate, SerialPort.DATABITS_7, SerialPort.STOPBITS_2, SerialPort.PARITY_EVEN);

        return serialPort;
    }

    /**
     * 关闭串口
     */
    public void closeComPort(SerialPort closeSerialPort) {

        if (closeSerialPort != null) {
            closeSerialPort.close();
        }
    }

    /**
     *
     * 接受消息
     *
     *
     */
    public String receiveMessage() {
        byte[] dis = null;
        String reS= null;
        InputStream inputStream = null;
        try {
            Thread.sleep(200);
            inputStream = serialPort.getInputStream();
            int length = inputStream.available();
            dis = new byte[length];
            while (length != 0) {
                inputStream.read(dis);
                length = inputStream.available();
            }
            reS = new String(dis,0,dis.length,"utf-8");
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return reS;
    }

    /**
     *
     * 发送消息
     *
     */
    public void sendMessage(String info) {
        info="@00RD0010000255*\r";
        byte[] by = null;
        OutputStream outputStream = null;
        try {
            outputStream = serialPort.getOutputStream();
            by = info.getBytes();
            outputStream.write(by);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("发送的字节数:" + info.getBytes().length + "内容:" + Arrays.toString(by));
    }

    /**
     *
     * 获取所有可用串口名字
     *
     */
    public List<String> getAllAvailbleCom() {
        return coms;
    }

    /**
     *
     * 获取所有串口集合
     */
    public List<CommPortIdentifier> getComPortList() {
        return comPorts;
    }

    public static void main(String[] args) {
        SerialPortService serialPortService = new SerialPortService();
        for(String msg:serialPortService.getAllAvailbleCom()){
            System.out.println(msg);
        }
    }


}
