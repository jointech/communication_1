
package  test.socket;



import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class ReceiveThread implements Runnable{

private Socket socket;

private DataInputStream inputStream;

    public ReceiveThread(Socket socket) {
        this.socket = socket;
        try {
            inputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
      while (true){
        receive();
      }
    }

    public  void receive(){
            try {
                byte[] bys = new byte[1024];
                int len = 0;
              String msg = inputStream.readUTF();
                System.out.println("客户端接收："+msg);
            } catch(IOException e){
                e.printStackTrace();
            }


    }
}
