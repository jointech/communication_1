package test.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TcpClient2 {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost",8888);

            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            System.out.println("客户端发送:数据");
            dos.writeUTF("数据");
            dos.flush();
            String msg = dis.readUTF();
            System.out.println("接收数据:"+msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
