package test.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TcpServer  {

    private ServerSocket serverSocket;

    public  static  int count;

    public List<ServerThread> clientList = new ArrayList<>();

    public void start(){
        try {
            serverSocket = new ServerSocket(8888);
            System.out.println("服务端已经启动等待连接~~~~~~~~~~~~~~~~~~~端口：8888");
            while(true){
                Socket socket = serverSocket.accept();
                count++;
                ServerThread serverThread = new ServerThread(socket,count+"AAA"+count);
                Thread thread = new Thread(serverThread);
                System.out.println("连接数:"+count);
                clientList.add(serverThread);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TcpServer tcpServer = new TcpServer();
        tcpServer.start();
    }


}
