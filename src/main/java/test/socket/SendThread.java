package test.socket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class SendThread implements Runnable {



    private Socket socket;

    private DataOutputStream outputStream;

    public SendThread(Socket socket) {
        this.socket = socket;
        try {
            outputStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        while (true) {

            send();
        }
    }

    public void  send() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
                outputStream.writeUTF("你好啊！大哥");
                System.out.println("客户端发送：你好啊！大哥");
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
