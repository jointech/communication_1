package test.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TcpClient {

    private InputStream inputStream;

    private OutputStream outputStream;

    private Socket socket;

    public  void start(){
        try {

                 socket = new Socket("localhost", 8888);
                 ReceiveThread receiveThread = new ReceiveThread(socket);
                 SendThread sendThread = new SendThread(socket);
                 Thread  re = new Thread(receiveThread);
                 Thread se = new Thread(sendThread);
                 re.start();
                 se.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
      TcpClient tcpClient =   new TcpClient();
      tcpClient.start();
    }
}



