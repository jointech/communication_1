package test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.ByteUtil;
import util.CRC;

import javax.xml.ws.RequestWrapper;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/13
 */
public class ToolTest {

    private static Logger logger = LoggerFactory.getLogger(ToolTest.class);

    private static final String intervalChar = " ";
    @Test
    public void testCRC(){
                                 //35
        String hexStr = "01 03 06 00 01 00 02 00 03".replace(" ","");
        String crc = CRC.getCRC(ByteUtil.hexStr2Byte(hexStr)).toUpperCase();
        System.out.println(crc);
        System.out.println(Integer.parseInt(crc,16));

    }
    @Test
    public void test(){
        String hexStr ="01030000000305";
        System.out.println(CRC.getCRC3(ByteUtil.hexStr2Byte(hexStr)));
        System.out.println(CRC.getCRC(ByteUtil.hexStr2Byte(hexStr)));
        System.out.println(CRC.getCRC(ByteUtil.hexStr2Byte(hexStr)));
        int CRC = 53429;
         CRC = ( (CRC & 0x0000FF00) >> 8) | ( (CRC & 0x000000FF ) << 8);
        System.out.println(Integer.toHexString(CRC).toUpperCase());

        System.out.println(Arrays.toString(ByteUtil.hexStr2Byte("1122")));
    }
    @Test
    public  void test4(){
        String hexStr ="01 03 02 00 01".replace(" ","");
        byte[] bytes = ByteUtil.hexStr2Byte(hexStr);
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000A001;
        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        System.out.println("互换前:"+Integer.toHexString(CRC).toUpperCase());

        CRC = ( (CRC & 0x0000FF00) >> 8) | ( (CRC & 0x000000FF ) << 8);

        String resCRC = Integer.toHexString(CRC);
        if(resCRC.length()==3){
            resCRC = "0"+resCRC;
        }

        System.out.println(resCRC);

    }

    @Test
    public void test6(){
        int res = 0x00008005;
        byte[] bys = ByteUtil.hexStr2Byte("22");
        System.out.println(bys.length);
        int  crc= (int) bys[0]<<16;
        for (int i = 0; i < 8; i++) {
                if ((crc & 0x100000) != 0) {
                    crc =  (crc ^ res);
                    crc =  (crc << 1);

                } else {
                    crc =(crc << 1);
                }

        }
        String CRC = Integer.toHexString(crc).toUpperCase();
        //CRC =  CRC.length() == 3 ? "0"+CRC : CRC;
        System.out.println(CRC);

        int a = 0x2A;
        int b = 0x0B;
        System.out.println(a&b);
        System.out.println();
        System.out.println(0b101010);
    }
    @Test
    public void test7(){
        String hexStr = "03";
        System.out.println(Arrays.toString(ByteUtil.hexStr2Byte(hexStr)));
    }
    @Test
    public void stringTest(){
        String str = "0123456789";
        System.out.println(str.substring(3,2));
        System.out.println("~~~~~~~~~~~~");
        System.out.println(str.substring(str.length()-2));
    }
    @Test
    public void testException(){
        try {
            InputStream inputStream = new FileInputStream("src/main/resources/a.txt");
            String str = "0123456789";
            System.out.println(str.substring(3,2));

        }catch (IOException e){

        }

    }

    public static void main(String[] args) {
        try {
            InputStream inputStream = new FileInputStream("src/main/resources/a.txt");
            String str = "0123456789";
            logger.info("fgsdfg");
            System.out.println(str.substring(3,2));


        }catch (IOException e){

        }
    }
    @Test
public  void testCount(){
  String msg = null;
  msg = "hello";
        System.out.println(msg.getBytes().toString());

}
@Test
public void transfer(){

        float a = 32.375f;

    System.out.println(Float.intBitsToFloat(0x42018000));
    //System.out.println(Float);
  }
  @Test
  public void swap(){

        int q = 0x00001234;
        Integer q2 =((q & 0x0000ff00)>>8) |((q & 0x000000ff)<<8);
      System.out.println(Integer.toHexString(q2));
  }





}
