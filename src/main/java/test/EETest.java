package test;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bingo
 * @Description
 * @Date 2018/9/1
 */
public class EETest {

    private List<Integer> list = new ArrayList<>();

    {
        list.add(1);
        list.add(2);
    }

    public  void add(){
        list  = new ArrayList<>();
        list.add(30);
        list.add(40);
        for (Integer item:list){
            System.out.print(item);
        }
        System.out.println();
    }


    public  void get(){
        for (Integer item:list){
            System.out.print(item);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        EETest eeTest  = new EETest();
        eeTest.add();
        eeTest.get();
    }
    @Test
    public void test(){
        System.out.println(0x42018000);
        //0 ‭1000010 0 000‬‭0001‬  ‭10000000‬ 00000000

        //n=1/2^7+1/2^8==> 1/128+1/256==>2/256+1/256===>3/256
    }

    @Test
    public void testFileCpopy(){

        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File("movie/1.mp3")));
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File("movie/copy.mp3")));
            byte[] bys = new byte[1024];
            int len = 0;
            while ((len=bis.read(bys))!=-1){
                bos.write(bys,0,len);
                bos.flush();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
