package test;

import module.modbus.rtu.RtuStartThread;
import module.modbus.rtu.RtuStopThread;
import org.junit.Test;


import java.util.Random;


/**
 * @author bingo
 * @Description
 * @Date 2018/8/29
 */
public class CRCTest {
    public static void main(String[] args) {

        RtuStartThread rtuStartThread = new RtuStartThread();
        RtuStopThread rtuStopThread = new RtuStopThread(rtuStartThread.getSerialPortRtu());

        Thread startThread = new Thread(rtuStartThread);
        Thread stopThread = new Thread(rtuStopThread);

        startThread.start();




    }
    @Test
    public  void test(){
        int[] slaveId = new int[]{1,2,3,4,5,6,7,8,9,10};
        int[] methodCode = new int[]{3,6,16};
        int[] rangeArr = new int[]{1,2,3,4,5,6,7,8,9,10};

        Random random = new Random();
        for(int i=0;i<50;i++){
            int slave = random.nextInt(slaveId.length);
            int code = random.nextInt(methodCode.length);
            int range = random.nextInt(rangeArr.length);
            System.out.println(slaveId[slave]+"----"+methodCode[code]+"----"+rangeArr[range]);
        }
    }

    @Test
    public void hlSwap(){
         int CRC = 0x607;
         CRC = ( (CRC & 0x0000FF00) >> 8) | ( (CRC & 0x000000FF ) << 8);
        System.out.println(Integer.toHexString(CRC));
    }


}
