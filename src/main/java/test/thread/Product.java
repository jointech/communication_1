package test.thread;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class Product implements Runnable {

    private Mmall mall;

    public Product(Mmall mall) {
        this.mall = mall;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mall.add();
        }

    }
}
