package test.thread;
/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class Mmall {

    public int count=0;

    public static  int MAX = 1000;


    public synchronized void  add(){
        System.out.println(Thread.currentThread().getName()+"开始生产");
         while (count>=MAX){
             try {
                 wait();
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }

         }
        count++;
        notify();
        System.out.println(Thread.currentThread().getName()+"生产完毕!当前库存"+count);
    }

    public synchronized void dec(){
        System.out.println(Thread.currentThread().getName()+"开始消费");
        while (count<=0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        count--;
        notify();
        System.out.println(Thread.currentThread().getName()+"消费完毕!当前库存"+count);
    }


    public static void main(String[] args) {
        Mmall mmall = new Mmall();
        Product product = new Product(mmall);
        Consume consume = new Consume(mmall);
        Thread t1 = new Thread(product);
        Thread t2 = new Thread(product);
        Thread t3 = new Thread(consume);


        t1.start();
        t2.start();
        t3.start();

    }
}
