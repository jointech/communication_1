package test.thread;

/**
 * @author bingo
 * @Description
 * @Date 2018/8/23
 */
public class Consume implements Runnable {

    private Mmall mall;

    public Consume(Mmall mall) {
        this.mall = mall;
    }

    @Override
     public void run() {
        while (true){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mall.dec();
        }

        }


    }

