package util;

import gnu.io.CommPortIdentifier;

import java.util.Enumeration;

/**
 * @author bingo
 * @Description        串口工具类
 * @Date 2018/8/17
 */
public class SerialTool {

    public  static  CommPortIdentifier getCommPortIdentifier(String port){
        CommPortIdentifier commPortIdentifier;
        Enumeration<CommPortIdentifier> comPortList = CommPortIdentifier.getPortIdentifiers();
        int i = 0;
        while (comPortList.hasMoreElements()) {
            commPortIdentifier = comPortList.nextElement();
            if(port.equals(commPortIdentifier.getName())){
                return  commPortIdentifier;
            }
        }
        return  null;
    }
}
