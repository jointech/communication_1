package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * @author bingo
 * @Description
 * @Date 2018/9/2
 */
public class FloatUtil {

    private static Logger logger = LoggerFactory.getLogger(FloatUtil.class);
    public static  double transfer(String hexStr){
        StringBuffer  binaryStr = new StringBuffer();
        for(int i=0;i< hexStr.length();i+=2){
            String a = hexStr.substring(i,i+2);
            int c = Integer.parseInt(a,16);
            String item = String.format("%08d",Integer.parseInt(Integer.toBinaryString(c)));
            binaryStr.append(item);
        }
        int n =  Integer.parseInt(binaryStr.substring(1,9),2);
        String mStr = binaryStr.substring(9,binaryStr.length()-1);
        double sum = 0;
        for(int i =1;i<=mStr.length();i++){
            if(mStr.charAt(i-1)=='1'){
                sum = sum+Math.pow(0.5,i);
            }
        }
        double a =( Math.pow(2,n-127))*(1+sum);
        return  a;
    }

    public static void main(String[] args) {
        System.out.println(FloatUtil.transfer("C1B9D70A"));
        System.out.println(Math.pow(0.5,3));
        System.out.println(Math.pow(-1,3));
    }
}