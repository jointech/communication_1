package util;

import org.junit.Test;
import redis.clients.jedis.Jedis;

/**
 * @author bingo
 * @Description
 * @Date 2018/9/6
 */
public class JedisTool {

    @Test
    public  void test() {
       long startTime = System.currentTimeMillis();
        for(int i=0;i<100;i++){
            Jedis jedis = new Jedis("localhost");
            jedis.set("k"+i,String.valueOf(i));
            jedis.close();
        }
        long endTime = System.currentTimeMillis();

        System.out.println((endTime-startTime));

    }
    @Test
    public void test2(){
        long startTime = System.currentTimeMillis();

        int count = 0;
        while (true){
            count++;
            Jedis jedis = new Jedis("localhost");
            jedis.get("k1");

            long currentTime = System.currentTimeMillis();
            if(currentTime-startTime>=1000){
                break;
            }
            jedis.close();
        }


        System.out.println("总数:"+count);

    }
    @Test
    public  void  executeInsert(){
        Jedis jedis = new Jedis("localhost");
        for(int i=0;i<1000000;i++){
            jedis.set("k"+i,String.valueOf(i));
        }
        jedis.close();
    }
    @Test
    public void testConnect(){
        Jedis jedis = new Jedis("localhost");

        System.out.println(jedis.ping());

        jedis.close();
    }


}
