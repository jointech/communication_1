package util;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @author bingo
 * @Description     和校验
 * @Date 2018/8/12
 */
public class LRC {

    private static  final int  CON = 256;

    public  static  String calcute(String msg ) {
        int len = msg.length();
        int res = 0;
        for (int i = 0; i < len / 2; i++) {
            String hexStr = "";
            if (i == len - 1) {
                hexStr = msg.substring(2 * i);
            } else {
                hexStr = msg.substring(2 * i, 2 * i + 2);
            }
            res += Integer.parseInt(hexStr, 16);
        }
        int mod = res % CON;
        int code = CON - mod;
        return Integer.toHexString(code).toUpperCase();
    }
}
